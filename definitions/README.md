# What is this?

[updates.json]（updates.json） 文件是更新定义文件。换句话说，它列出了每个平台的最新版本、更新日志和下载 URL。

示例项目下载 [updates.json]（updates.json） 文件并进行分析，以便：

- 比较本地（用户集）版本和远程版本（在 [updates.json]（updates.json） 中指定）
- 下载当前平台的更改日志
- 获取下载地址
- 获取要打开的 URL（如果指定）

有关更多信息，请查看 [wiki]（#） 上关于本文的文章